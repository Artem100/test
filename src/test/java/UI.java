import com.codeborne.selenide.Configuration;
import com.sun.org.glassfish.gmbal.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.CreateAccount;
import pages.Main;

import static com.codeborne.selenide.Selenide.open;

public class UI {

    private static Main main;
    private static CreateAccount createAccount;

    @BeforeMethod
    public void setup (){
        main = new Main();
        createAccount = new CreateAccount();
        Configuration.browser="chrome";
        Configuration.timeout = 4000;
        open("https://www.amazon.com");
    }

    @Description("Positive test")
    @Test
    public void firstTest(){
        main.createAccount();
        createAccount.trylook();
    }
}
