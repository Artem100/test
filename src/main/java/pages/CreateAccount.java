package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class CreateAccount {

    private By fieldYourName = By.id("ap_customer_name");

    public boolean assertMainPage(){
        return title().contains("Amazon Registration");
    }

    public void trylook(){
        $(fieldYourName).click();
    }
}
