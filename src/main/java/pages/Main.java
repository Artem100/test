package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class Main {


    private By panelAccountList = By.id("nav-link-accountList");
    private By buttonStartHere = By.cssSelector(".nav-signin-tooltip-footer > a");
    private By panelSignIn = By.cssSelector("div.nav-signin-tt.nav-flyout");


    public boolean assertMainPage(){
        return title().contains("Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more");
    }

    public void createAccount(){
        $(panelSignIn).isDisplayed();
        $(buttonStartHere).click();
    }



}
